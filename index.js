const path = require('path');
const url = require('url');

const { app, BrowserWindow, Menu, ipcMain } = require('electron');

let win;

function createWindow() {
    win = new BrowserWindow({
        width: 700,
        height: 500
    });

    win.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file',
        slashes: true
    }));

    win.webContents.openDevTools();

    win.on('closed', () => {
        win = null;
    });
}
app.on('ready', () => {
    createWindow
    dashboard = new BrowserWindow();

    dashboard.loadURL(url.format({
        pathname: path.join(__dirname, '/views/index.html'),
        protocol: 'file:',
        slashes: true
    }));

    dashboard.on('close', function () {
        app.quit();
    });

    const menu = Menu.buildFromTemplate(menuTemplate);

    Menu.setApplicationMenu(menu);
});
app.on('window-all-closed', () => {
    app.quit();
})

const menuTemplate = [
    {
        label: 'Q',
        submenu: [
            {
                label: 'Quit',
                accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
                click() {
                    app.quit();
                }
            }
        ]
    },
    {
        label: 'Actions',
        submenu: [
            {
                label: 'New Todo',
                accelerator: process.platform == 'darwin' ? 'Command+N' : 'Ctrl+N',
                click() {
                    if (!todoWindow) {
                        openNewTodo();
                    }
                }
            }
        ]
    }
]

